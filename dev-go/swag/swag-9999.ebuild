# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit go-module

DESCRIPTION="Swagger 2.0 documentation generator for Go"
HOMEPAGE="https://github.com/swaggo/swag"
LICENSE="MIT Apache-2.0 BSD"
SLOT="0"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/swaggo/swag.git"

	src_unpack() {
		git-r3_src_unpack
		go-module_live_vendor
	}
else
	SRC_URI="https://github.com/swaggo/swag/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	SRC_URI+=" https://git.froth.zone/api/packages/packaging/generic/portage/deps/${P}-vendor.tar.xz"
	KEYWORDS="~amd64 ~arm64 ~x86"
fi

src_compile() {
	ego build ./cmd/swag
}

src_install() {
	dobin ./${PN}
	einstalldocs
}
