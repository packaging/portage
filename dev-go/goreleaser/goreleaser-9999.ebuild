# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit go-module

DESCRIPTION="Deliver Go binaries as fast and easily as possible"
HOMEPAGE="https://github.com/goreleaser/goreleaser https://goreleaser.com/"
LICENSE="MIT BSD BSD-2 Apache-2.0 MPL-2.0"
SLOT="0"

PATCHES=(
	"${FILESDIR}/${PN}-manpage-no-compress.patch"
)

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/goreleaser/goreleaser.git"

	src_unpack() {
		git-r3_src_unpack
		go-module_live_vendor
	        COMMIT=$(git-r3_peek_remote_ref)

	}
else
	# TODO: UPDATE EVERY BUMP
	COMMIT="6a7a9ba3995728a4bdf7b5fff770d8caeb3d2cc8"
	SRC_URI="https://github.com/goreleaser/goreleaser/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	SRC_URI+=" https://git.froth.zone/api/packages/packaging/generic/portage/deps/${P}-vendor.tar.xz"
	KEYWORDS="~amd64 ~arm64 ~x86"
fi

src_compile() {
	ego build  \
	-ldflags "-s -w -X main.version=${PV} -X main.date=$(date -I) -X main.commit=${COMMIT} -X main.builtBy=portage" .
	./scripts/manpages.sh
}

src_install() {
	dobin ./${PN}
	doman manpages/goreleaser.1
	einstalldocs
}
