# froth.zone portage
Here lies the "official" froth.zone portage overlay.

Basically all that is here is some stuff I use regularly.

To add this simply add the following line as `root`:
```sh
eselect repository add froth-zone git https://git.froth.zone/packaging/portage.git
```

I make 0 guarantees that any of this has or will ever work.

## PROCEED WITH CAUTION
