# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit go-module

DESCRIPTION="TUI for SSH"
HOMEPAGE="https://github.com/quantumsheep/sshs"
LICENSE="MIT GPL-3+ Apache-2.0 BSD"
SLOT="0"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/quantumsheep/sshs.git"

	src_unpack() {
		git-r3_src_unpack
		go-module_live_vendor
	}
else
	SRC_URI="https://github.com/quantumsheep/sshs/archive/${PV}.tar.gz -> ${P}.tar.gz"
	SRC_URI+=" https://git.froth.zone/api/packages/packaging/generic/portage/deps/${P}-vendor.tar.xz"
	KEYWORDS="~amd64 ~arm64 ~x86"
fi

src_compile() {
	emake PREFIX="${EPREFIX}/usr"
}

src_install() {
	emake PREFIX="${EPREFIX}/usr" DESTDIR="${ED}" install
	einstalldocs
}
