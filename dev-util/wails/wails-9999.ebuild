# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit go-module

DESCRIPTION="Create beautiful applications using Go and JavaScript"
HOMEPAGE="https://github.com/wailsapp/wails https://wails.io/"
LICENSE="MIT BSD BSD-2 Apache-2.0 MPL-2.0"
SLOT="0"

RDEPEND="sys-devel/gcc
				 net-libs/webkit-gtk
				 net-libs/nodejs
				 dev-util/pkgconf"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	# Override the default path to v3
	S="${WORKDIR}/${P}/v3"
	EGIT_REPO_URI="https://github.com/wailsapp/wails.git"
	EGIT_BRANCH="v3-alpha"
	PROGMAJ="3"

	src_unpack() {
		git-r3_src_unpack
		go-module_live_vendor
	}
else
	SRC_URI="https://github.com/wailsapp/wails/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	SRC_URI+=" https://git.froth.zone/api/packages/packaging/generic/portage/deps/${P}-vendor.tar.xz"
	KEYWORDS="~amd64 ~arm64 ~x86"
	# Override the default path to v2
	S="${WORKDIR}/${P}/v2"
	PROGMAJ=""
fi

src_compile() {
	ego build -ldflags "-s -w" ./cmd/wails${PROGMAJ}
}

src_install() {
	dobin ./${PN}${PROGMAJ}
	einstalldocs
}
