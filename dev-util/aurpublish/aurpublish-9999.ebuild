# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="PKGBUILD management framework for the Arch User Repository"
HOMEPAGE="https://github.com/eli-schwartz/aurpublish"
LICENSE="GPL-2"
SLOT="0"

RDEPEND="sys-apps/pacman"

inherit git-r3
EGIT_REPO_URI="https://github.com/eli-schwartz/aurpublish"

src_unpack() {
	git-r3_src_unpack
}

src_compile() {
	emake PREFIX="${EPREFIX}/usr"
}

src_install() {
	emake PREFIX="${EPREFIX}/usr" DESTDIR="${ED}" install
	einstalldocs
}
