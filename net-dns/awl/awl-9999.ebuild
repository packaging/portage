# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit go-module

DESCRIPTION="command-line DNS query client"
HOMEPAGE="https://dns.froth.zone/awl"

LICENSE="BSD MIT Unlicense CC0-1.0"
SLOT="0"
# test requires network for making DNS queries
RESTRICT="test"
#IUSE="gccgo"
BDEPEND="app-text/scdoc"
#					gccgo? ( >=sys-devel/gcc-11[go] )"
#

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://git.froth.zone/sam/awl.git"

	src_unpack() {
		git-r3_src_unpack
		go-module_live_vendor
	}

	src_configure() {
#		if use gccgo; then
#			# -buildmode=pie is not supported by gccgo
#			GOFLAGS="-buildvcs=false -modcacherw -v -x -compiler=gccgo"
#		else
			GOFLAGS+=" -ldflags=-s -ldflags=-w -ldflags=-X=main.version=$(git-r3_peek_remote_ref)"
#		fi
	}
else
	SRC_URI="https://git.froth.zone/sam/awl/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	SRC_URI+=" https://git.froth.zone/sam/awl/releases/download/v${PV}/${P}-deps.tar.xz"
	KEYWORDS="~amd64 ~arm ~arm64 ~loong ~mips ~ppc64 ~riscv ~x86"

	# Gitea and only Gitea puts their tarballs in weird places
	S="${WORKDIR}/awl"

	src_configure() {
#		if use gccgo; then
#			# -buildmode=pie is not supported by gccgo
#			GOFLAGS="-buildvcs=false -modcacherw -v -x -compiler=gccgo"
#		else
			GOFLAGS+=" -ldflags=-s -ldflags=-w -ldflags=-X=main.version=${PV}"
#		fi
	}
fi

src_compile() {
	emake PREFIX="${EPREFIX}/usr"
}

src_test() {
	#-buildmode=pie clashes with -race
	emake test GOFLAGS="-buildvcs=false -modcacherw -v -x"
}

src_install() {
	emake PREFIX="${EPREFIX}/usr" DESTDIR="${ED}" install
	einstalldocs
}
